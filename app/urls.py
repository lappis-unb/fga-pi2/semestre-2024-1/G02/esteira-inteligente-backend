from django.urls import re_path, path
from . import views

urlpatterns = [
    path('usuarios/', views.UsuarioList.as_view(), name='usuario-list'),
    path('usuarios/<int:usuario_id>/', views.UsuarioDetail.as_view(), name='usuario-detail'),
    re_path(r'^cargos/$', views.CargoList.as_view(), name='cargo-list'),
    re_path(r'^tipos/$', views.TipoList.as_view(), name='tipo-list'),
    re_path(r'^componentes/$', views.ComponenteList.as_view(), name='componente-list'),
    re_path(r'^kits/$', views.KitList.as_view(), name='kit-list'),
    re_path(r'^kit-componentes/$', views.KitComponenteList.as_view(), name='kitcomponente-list'),
    re_path(r'^historico-producao/$', views.HistoricoProducaoList.as_view(), name='historicoproducao-list'),
    path('consumir-servico-ia/', views.ConsumirServicoIA.as_view(), name='consumir-servico-ia'),
    path('kits-agrupados/', views.KitsAgrupados.as_view(), name='kits-agrupados-list'),
    path('send-code/<int:gcode_id>/', views.GCodeSender.as_view(), name='gcode'),
    path('read-scale-data/', views.ReadScaleData.as_view(), name='read-scale-data')
]
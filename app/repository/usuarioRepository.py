from ..models import Usuario

class UsuarioRepository:
  @staticmethod
  def getAllUsuario():
    return Usuario.objects.all();

  @staticmethod
  def getUsuarioById(usuario_id):
    try:
        return Usuario.objects.get(id=usuario_id)
    except Usuario.DoesNotExist:
        return None

  
  @staticmethod
  def createUsuario(data):
    return Usuario.objects.create(**data)
  
  @staticmethod
  def updateUsuario(data):
    return Usuario.objects.update()
  
  @staticmethod
  def deleteUsuario(usuario_id):
    usuario = Usuario.objects.get(id=usuario_id)
    return usuario.delete()
; Define as unidades em milímetros e o modo de posicionamento relativo
G21 ; Unidades em milímetros
G91 ; Posicionamento relativo

; Define a velocidade de movimento (F = 3600 mm/min)
F3600 ; Velocidade de avanço

; Movimenta cada eixo em 10 mm
G1 X20 ; Movimenta o eixo X 10 cm para a direita
G1 Y20 ; Movimenta o eixo Y 10 cm para frente
G1 Z20 ; Movimenta o eixo Z 10 cm para cima

; Finaliza o programa




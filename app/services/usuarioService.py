from ..repository.usuarioRepository import UsuarioRepository

class UsuarioService:
  @staticmethod
  def getAllUsuario():
    return UsuarioRepository.getAllUsuario()
  
  @staticmethod
  def getUsuarioById(usuario_id):
    return UsuarioRepository.getUsuarioById(usuario_id)
  
  @staticmethod
  def createUsuario(data):
    return UsuarioRepository.createUsuario(data)
  
  def deleteUsuario(usuario_id):
    return UsuarioRepository.deleteUsuario(usuario_id)
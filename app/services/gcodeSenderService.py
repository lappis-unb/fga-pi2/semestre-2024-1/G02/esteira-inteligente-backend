import serial
import time
from serial.tools import list_ports

class GCodeSenderService:
    def sendGCode(self, port, file_path):
        def removeComment(string):
            if (string.find(';') == -1):
                return string
            else:
                return string[:string.index(';')]
        
        ports = self.listSerialPorts()
        print(ports)
        try:
            # Open serial port
            s = serial.Serial(port, 115200)
            print(f'Opening Serial Port {port}')
        except serial.SerialException as e:
            print(f'Error opening serial port: {str(e)}')
            raise

        try:
            # Open g-code file
            with open(file_path, 'r') as f:
                print('Opening gcode file')

                # Wake up
                s.write(b"\r\n\r\n")  # Hit enter a few times to wake the Printrbot
                time.sleep(2)  # Wait for Printrbot to initialize
                s.flushInput()  # Flush startup text in serial input
                print('Sending gcode')

                # Stream g-code
                for line in f:
                    l = removeComment(line)
                    l = l.strip()  # Strip all EOL characters for streaming
                    if l.isspace() == False and len(l) > 0:
                        print('Sending: ' + l)
                        s.write((l + '\n').encode())  # Send g-code block
                        grbl_out = s.readline()  # Wait for response with carriage return
                        print(' : ' + grbl_out.strip().decode())
        except FileNotFoundError:
            print(f'Gcode file {file_path} not found')
            raise
        except Exception as e:
            print(f'Error reading or sending gcode file: {str(e)}')
            raise
        finally:
            s.close()
            print('Gcode sent and serial port closed')

    @staticmethod
    def listSerialPorts():
        ports = list_ports.comports()
        return [port.device for port in ports]

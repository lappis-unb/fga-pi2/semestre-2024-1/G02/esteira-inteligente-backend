# services/serial_service.py

import serial
from serial.tools import list_ports

class SerialService:
    def __init__(self, port, baudrate=9600, timeout=1):
        self.port = port
        self.baudrate = baudrate
        self.timeout = timeout
        self.serial_connection = serial.Serial(port, baudrate, timeout=timeout)

    def read_data(self):
        if self.serial_connection.is_open:
            data = self.serial_connection.readline().decode('utf-8').strip()
            return data
        else:
            raise Exception("Serial connection is not open")

    def send_command(self, command):
        if self.serial_connection.is_open:
            self.serial_connection.write(command.encode('utf-8'))
        else:
            raise Exception("Serial connection is not open")

    def close(self):
        if self.serial_connection.is_open:
            self.serial_connection.close()

    @staticmethod
    def listSerialPorts():
        ports = list_ports.comports()
        return [port.device for port in ports]

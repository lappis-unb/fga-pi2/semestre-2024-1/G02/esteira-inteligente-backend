import os
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.utils import timezone
import requests
import base64
import json
from .models import *
from .serializers import *
from .services.gcodeSenderService import GCodeSenderService
from .services.serialService import SerialService
from .services.usuarioService import UsuarioService

IA_API_URL = "http://127.0.0.1:8080/IA"

# Create your views here.from 
class UsuarioList(APIView):
    def get(self, request):
        usuarios = UsuarioService.getAllUsuario()
        serializer = UsuarioSerializer(usuarios, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = UsuarioSerializer(data=request.data)
        if serializer.is_valid():
            UsuarioService.createUsuario(serializer.validated_data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UsuarioDetail(APIView):
    def get(self, request, usuario_id):
        usuario = UsuarioService.getUsuarioById(usuario_id)
        if usuario:
            serializer = UsuarioSerializer(usuario)
            return Response(serializer.data)
        return Response({"error": "Usuario não encontrado"}, status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, usuario_id):
        try:
            UsuarioService.deleteUsuario(usuario_id)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except:  
          return Response({"error": "Usuario não encontrado"}, status=status.HTTP_404_NOT_FOUND)

        
class CargoList(generics.ListCreateAPIView):
    queryset = Cargo.objects.all()
    serializer_class = CargoSerializer

class TipoList(generics.ListCreateAPIView):
    queryset = Tipo.objects.all()
    serializer_class = TipoSerializer

class ComponenteList(generics.ListCreateAPIView):
    queryset = Componente.objects.all()
    serializer_class = ComponenteSerializer

class KitList(generics.ListCreateAPIView):
    queryset = Kit.objects.all()
    serializer_class = KitSerializer

class KitComponenteList(generics.ListCreateAPIView):
    queryset = KitComponente.objects.all()
    serializer_class = KitComponenteSerializer

class HistoricoProducaoList(generics.ListCreateAPIView):
    queryset = HistoricoProducao.objects.all()
    serializer_class = HistoricoProducaoSerializer

    def perform_create(self, serializer):
        serializer.save(hora_producao=timezone.now())

class ConsumirServicoIA(APIView):
    def get(self, request, format=None):
        try:
            response = requests.get(IA_API_URL)

            if response.status_code == 200:
                data = json.loads(response.content)

                decoded_binary_data_image = base64.b64decode(data["image"])
                classes_identificadas = data.get("classesIdentificadas", [])
                confianca = data.get("confianca", [])

                with open('restored_image.jpg', 'wb') as file:
                    file.write(decoded_binary_data_image)
                return Response({"classes_identificadas": classes_identificadas, "confianca": confianca }, status=status.HTTP_200_OK)
            else:
                return Response({"error": "Erro ao acessar a API de IA"}, status=response.status_code)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
class KitsAgrupados(APIView):
    def get(self, request, format=None):
        kits_agrupados = []
        kits = Kit.objects.all()

        for kit in kits:
            composicoes_kit = KitComponente.objects.filter(id_kit=kit)
            quantidade_peca_total = 0
            componentes = []

            for composicao_kit in composicoes_kit:
                componente = composicao_kit.id_componente
                quantidade = composicao_kit.qtd_pecas
                quantidade_peca_total += quantidade
                componentes.append({
                    'id_componente': componente.id,
                    'nome': componente.nome,
                    'tipo': componente.id_tipo.nome,
                    'quantidade': quantidade
                })

            kits_agrupados.append({
                'id_kit': kit.id,
                'kit_nome': kit.nome,
                'componentes': componentes,
                'quantidade_peca_total': quantidade_peca_total
            })

        return Response(kits_agrupados)
class GCodeSender(APIView):
    def get(self, request, gcode_id):
        gcodesender_service = GCodeSenderService()
        port = "COM6" ## ajustar porta conforme OS

        # Mapeamento do gcode_id para os nomes dos arquivos
        gcode_files = {
            1: 'kit_1.cnc',
            2: 'kit_2.cnc',
            3: 'kit_3.cnc'
        }

        file_name = gcode_files.get(gcode_id)
        if not file_name:
            return Response({"error": f"GCode ID '{gcode_id}' is not valid"}, status=status.HTTP_400_BAD_REQUEST)

        file_path = os.path.join(os.path.dirname(__file__), 'gcodes', file_name)

        if not os.path.exists(file_path):
            return Response({"error": f"File '{file_path}' does not exist"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            gcodesender_service.sendGCode(port, file_path)
            return Response({"message": "GCode sent successfully"}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        
class ReadScaleData(APIView):
    def get(self, request, format=None):
        port = "COM8"  # Ajustar porta conforme necessário o SO...
        baudrate = 9600

        try:
            serial_service = SerialService(port, baudrate)
            data = serial_service.read_data()
            serial_service.close()
            return Response({"data": data}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
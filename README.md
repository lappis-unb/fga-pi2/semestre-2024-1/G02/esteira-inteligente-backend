# API Esteira Inteligente

Este projeto consiste em uma API desenvolvida com Django e Django Rest Framework (DRF), hospedada em uma Raspberry Pi. A API gerencia informações de usuários, cargos, componentes, tipos, kits e histórico de produção. Além disso, oferece funcionalidades adicionais, como consumo de um serviço de IA, envio de GCodes para uma máquina e leitura de dados de uma balança serial. O projeto também inclui um sistema de autenticação utilizando JWT.

## Tecnologias Utilizadas

- **Django Framework v5.0.2**
- **Django Rest Framework**
- **Python v3.10.10 ou superior**
- **Raspberry Pi 4 Model B ou superior**
- **JWT para autenticação**

## Requisitos

Certifique-se de ter as seguintes dependências instaladas:

- Python 3.10.10
- Django 5.0.2
- Django Rest Framework
- Outros pacotes listados em `requirements.txt`

## Instalação

1. Clone o repositório:

    ```bash
    git clone https://github.com/seu-usuario/seu-repositorio.git
    cd seu-repositorio
    ```

2. Crie um ambiente virtual e ative-o:

    ```bash
    python3 -m venv venv
    source venv/bin/activate
    ```

3. Instale as dependências:

    ```bash
    pip install -r requirements.txt
    ```

4. Configure o banco de dados e execute as migrações:

    ```bash
    python3 manage.py makemigrations
    python3 manage.py migrate
    ```

5. Popule o banco de dados com dados iniciais:

    ```bash
    python3 manage.py populate_db
    ```

## Executando o Servidor

Para iniciar o servidor, execute o seguinte comando:

```bash
python3 manage.py runserver
```

A API estará disponível em `http://localhost:8000`.

## Endpoints Disponíveis

### Autenticação

- **POST /api/token/**: Obtém um par de tokens (acesso e atualização).
- **POST /api/token/refresh/**: Atualiza um token de acesso usando um token de atualização.
- **GET /api/home/**: Exibe uma mensagem de boas-vindas (requer autenticação).
- **POST /api/logout/**: Faz logout do usuário, invalidando o token de atualização.

### Usuários

- **GET /api/usuarios/**: Lista todos os usuários.
- **POST /api/usuarios/**: Cria um novo usuário.
- **GET /api/usuarios/{usuario_id}/**: Obtém detalhes de um usuário específico.
- **DELETE /api/usuarios/{usuario_id}/**: Remove um usuário específico.

### Cargos

- **GET /api/cargos/**: Lista todos os cargos.
- **POST /api/cargos/**: Cria um novo cargo.

### Tipos

- **GET /api/tipos/**: Lista todos os tipos.
- **POST /api/tipos/**: Cria um novo tipo.

### Componentes

- **GET /api/componentes/**: Lista todos os componentes.
- **POST /api/componentes/**: Cria um novo componente.

### Kits

- **GET /api/kits/**: Lista todos os kits.
- **POST /api/kits/**: Cria um novo kit.

### Kit Componentes

- **GET /api/kit-componentes/**: Lista todos os componentes de kits.
- **POST /api/kit-componentes/**: Cria um novo componente de kit.

### Histórico de Produção

- **GET /api/historico-producao/**: Lista todo o histórico de produção.
- **POST /api/historico-producao/**: Cria um novo registro no histórico de produção.

### Funcionalidades Adicionais

- **GET /api/consumir-servico-ia/**: Consome um serviço de IA e retorna as classes identificadas e a confiança.
- **GET /api/kits-agrupados/**: Lista os kits agrupados com seus componentes e quantidades totais.
- **GET /api/send-code/{gcode_id}/**: Envia um código GCode para a máquina.
- **GET /api/read-scale-data/**: Lê dados de uma balança serial.



Se precisar de mais alguma coisa ou de mais detalhes, estou à disposição!